const ws = 'ws://' + socket_host + ':' + socket_port;
const bodyEl = document.getElementsByTagName('body')[0];
const resetEl = document.getElementById('reset');

const init = function () {
    const websocket = new WebSocket(ws);
    websocket.onerror = function() {
        alert('Failed to connect to web socket ' + ws);
        return false;
    };
    websocket.onopen = function() {
        websocket.send('admin');
        websocket.onmessage = function msg(e) {
            if ('reset' === e.data) {
                [].forEach.call(document.querySelectorAll('h1'),function(e){
                    e.parentNode.removeChild(e);
                });
            } else {
                const data = JSON.parse(e.data);
                let el = document.createElement('h1');
                el.style.color = '#' + data.color;
                el.innerText = data.position + ' - ' + data.name;
                bodyEl.append(el);
            }
        };
        resetEl.onclick = function reset() {
            if (1 !== websocket.readyState) {
                // try to reconnect
                window.document.dispatchEvent(new Event('DOMContentLoaded'));
            }
            websocket.send('reset');
            [].forEach.call(document.querySelectorAll('h1'),function(e){
                e.parentNode.removeChild(e);
            });
        };
        addEventListener('keypress', function (e) {
            if('Space' === e.code) {
                resetEl.click();
            }
        });
    };
};

if(document.readyState !== 'loading') {
    init(); // trick for DOMContentLoaded not working on Safari
} else {
    document.addEventListener('DOMContentLoaded', init);
}