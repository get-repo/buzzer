const ws = 'ws://' + socket_host + ':' + socket_port;
const bodyEl = document.getElementsByTagName('body')[0];
const buzzerEl = document.getElementById('buzzer');
const positionEl = document.getElementById('position');

const init = function (DOMContentEvent) {
    const websocket = new WebSocket(ws);
    websocket.onerror = function() {
        buzzer.remove();
        alert('Failed to connect to web socket ' + ws);
        return false;
    };
    websocket.onopen = function() {
        websocket.send('register:' + username + ':' + color);
        buzzerEl.onclick = function() {
            if (1 !== websocket.readyState) {
                // try to reconnect
                window.document.dispatchEvent(new Event('DOMContentLoaded'));
            }
            buzzerEl.disabled = true;
            buzzerEl.classList.add('disabled');
            websocket.send('press');
        }
        websocket.onmessage = function msg(e){
            const data = e.data;
            if ('reset' === data) {
                bodyEl.classList.remove('winner', 'too-late');
                buzzerEl.disabled = false;
                buzzerEl.classList.remove('disabled');
                positionEl.style.visibility = 'hidden';
                positionEl.innerHTML = 0;
            } else {
                bodyEl.classList.add(1 == data ? 'winner' : 'too-late');
                positionEl.innerHTML = data;
                positionEl.style.visibility = 'visible';
            }
        };
    };
};

if(document.readyState !== 'loading') {
    init(); // trick for DOMContentLoaded not working on Safari
} else {
    document.addEventListener('DOMContentLoaded', init);
}
