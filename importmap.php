<?php

return [
    'setup' => [
        'path' => './assets/setup.js',
        'entrypoint' => true,
    ],
    'client' => [
        'path' => './assets/client.js',
        'entrypoint' => true,
    ],
    'admin' => [
        'path' => './assets/admin.js',
        'entrypoint' => true,
    ],
];
