<?php

namespace App\Cache;

use Symfony\Component\Cache\CacheItem;
use Symfony\Contracts\Cache\CacheInterface;

class CacheHandler
{
    public function __construct(
        protected CacheInterface $buzzerCache,
    ) {
    }

    public function has(int $id, string $key): bool
    {
        $cacheItem = $this->getCacheItem((string)$id);

        return $cacheItem->isHit() && array_key_exists($key, $cacheItem->get());
    }

    public function get(int $id, string $key): mixed
    {
        return $this->has($id, $key) ? $this->getCacheItem((string)$id)->get()[$key] : null;
    }

    public function set(int $id, string $key, mixed $value): void
    {
        $cacheItem = $this->getCacheItem((string)$id);
        $array = $cacheItem->isHit() ? $cacheItem->get() : [];
        $array[$key] = $value;
        $cacheItem->set($array);
        $this->buzzerCache->save($cacheItem); // @phpstan-ignore-line
    }

    public function remove(int $id, string $key): void
    {
        if ($this->has($id, $key)) {
            $cacheItem = $this->getCacheItem((string)$id);
            $array = $cacheItem->isHit() ? $cacheItem->get() : [];
            unset($array[$key]);
            $cacheItem->set($array);
            $this->buzzerCache->save($cacheItem); // @phpstan-ignore-line
        }
    }

    public function clear(int $id): void
    {
        $cacheItem = $this->getCacheItem((string)$id);
        $cacheItem->set([]);
        $this->buzzerCache->save($cacheItem); // @phpstan-ignore-line
    }

    public function addAdminIdentifier(int $id): void
    {
        $cacheItem = $this->getCacheItem('admin');
        $array = $cacheItem->isHit() ? $cacheItem->get() : [];
        $array['ids'][] = $id;
        $cacheItem->set($array);
        $this->buzzerCache->save($cacheItem); // @phpstan-ignore-line
    }

    public function getAdminIdentifiers(): array
    {
        $cacheItem = $this->getCacheItem('admin');

        return $cacheItem->isHit() ? ($cacheItem->get()['ids'] ?? []) : [];
    }

    private function getCacheItem(string $name): CacheItem
    {
        return $this->buzzerCache->getItem($name); // @phpstan-ignore-line
    }
}
