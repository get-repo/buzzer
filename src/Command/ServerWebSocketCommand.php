<?php

namespace App\Command;

use App\WebSocket\BuzzerHandler;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

#[AsCommand(name: 'server:web-socket')]
class ServerWebSocketCommand extends Command
{
    public function __construct(
        private BuzzerHandler $buzzerHandler,
        #[Autowire(param: 'ratchet.web_socket_host')]
        private string $host,
        #[Autowire(param: 'ratchet.web_socket_port')]
        private int $port,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('debug', null, InputOption::VALUE_NONE, 'Display debug');
    }

    /**
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->getOption('debug')) {
            $this->buzzerHandler->setDebugOutput($output);
        }
        // run web socket server
        $webSocketServer = IoServer::factory(
            new HttpServer(new WsServer($this->buzzerHandler)),
            $this->port,
            $this->host,
        );

        pcntl_signal(SIGINT, function (): void {
            $this->buzzerHandler->clear();
            exit;
        });

        $output->writeLn(sprintf(
            'web socket server on port <fg=yellow>%s:%d</>',
            $this->host,
            $this->port,
        ));
        $webSocketServer->run();

        return Command::SUCCESS;
    }
}
