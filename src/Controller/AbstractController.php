<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseAbstractController;
use Symfony\Component\HttpFoundation\Response;

class AbstractController extends BaseAbstractController
{
    protected function template(array $parameters = []): Response
    {
        $templateDir = $this->getParameter('twig.default_path');
        $theme = $this->getParameter('buzzer_theme');
        if (!is_dir($templateDir = sprintf('%s/%s', $templateDir, $theme))) {
            throw new \RuntimeException(sprintf('Theme "%s" directory was not found in "%s"', $theme, $templateDir));
        }

        /** @var \Symfony\Component\HttpFoundation\RequestStack  $requestStack */
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getMainRequest();
        [$controller, $action] = explode('_', $request->attributes->get('_route'));

        return parent::render(
            sprintf('%s/%s/%s.html.twig', $theme, $controller, $action),
            $parameters,
        );
    }
}
