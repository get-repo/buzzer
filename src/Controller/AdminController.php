<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Attribute\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin_manage', condition: 'request.getClientIp() === "%buzzer_admin_ip%"')]
    public function manage(SessionInterface $session): Response
    {
        $session->set('admin', true);
        $session->save();

        return $this->template();
    }
}
