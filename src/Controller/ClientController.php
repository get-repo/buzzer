<?php

namespace App\Controller;

use App\Form\ClientNameFormType;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Cache\CacheInterface;

class ClientController extends AbstractController
{
    public function __construct(
        protected CacheInterface $buzzerCache,
        #[Autowire(param: 'buzzer_colors')]
        protected array $colors,
    ) {
    }

    #[Route('/', name: 'client_buzzer')]
    public function buzzer(SessionInterface $session): Response
    {
        if (!$session->has('name')) {
            return $this->redirectToRoute('client_setup');
        }

        return $this->template();
    }

    #[Route('/setup', name: 'client_setup', methods: ['GET', 'POST'])]
    public function setUp(Request $request): Response
    {
        $session = $request->getSession();
        if ($session->has('name')) {
            return $this->redirectToRoute('client_buzzer');
        }
        $form = $this->createForm(ClientNameFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // name
            $session->set('name', $form->get('name')->getData());
            // color
            $cacheItem = $this->buzzerCache->getItem('color_counter'); // @phpstan-ignore-line
            $i = 0;
            if ($cacheItem->isHit()) {
                $i = $cacheItem->get() + 1;
            }
            $cacheItem->set($i);
            $this->buzzerCache->save($cacheItem); // @phpstan-ignore-line
            $color = $this->colors[$i] ?? str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $session->set('color', $color);
            $session->save();

            return $this->redirectToRoute('client_buzzer');
        }

        return $this->template(['form' => $form]);
    }

    #[Route('/reset', name: 'client_reset')]
    public function reset(SessionInterface $session): Response
    {
        $session->clear();

        return $this->redirectToRoute('client_buzzer');
    }
}
