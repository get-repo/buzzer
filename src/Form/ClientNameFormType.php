<?php

namespace App\Form;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\Cache\CacheInterface;

class ClientNameFormType extends AbstractType
{
    public function __construct(
        protected CacheInterface $buzzerCache,
        #[Autowire(param: 'kernel.session_dir')]
        private string $sessionDir,
        #[Autowire(param: 'buzzer_colors')]
        protected array $colors,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'name',
            null,
            [
                'label' => false,
                'required' => true,
                'attr' => [
                    'placeholder' => 'What\'s your name ?',
                    'pattern' => '[a-zA-Z]+',
                    'minlength' => 3,
                    'maxlength' => 10,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Regex(pattern: '/^[a-z]+$/i', message: 'Must contain only letters.'),
                    new Assert\Length(min: 3, max: 10),
                    new Assert\Callback(function (?string $value, ExecutionContextInterface $context): void {
                        if (is_dir($this->sessionDir)) {
                            /** @var string $file */
                            foreach ((array) glob("{$this->sessionDir}/*") as $file) {
                                /** @var string $content */
                                $content = str_replace('_sf2_attributes|', '', (string)@file_get_contents($file));
                                if (is_array($content = @unserialize($content))) {
                                    if ($value === ($content['name'] ?? null)) {
                                        $context->buildViolation('This name is already used.')
                                            ->atPath('name')
                                            ->addViolation();
                                        break;
                                    }
                                }
                            }
                        }
                    }),
                ]
            ]
        );
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (PreSubmitEvent $event): void {
            $data = $event->getData();
            $data['name'] = strtoupper($data['name'] ?? '');
            $event->setData($data);
        });

        $builder->add('submit', SubmitType::class, ['label' => 'Let\'s go']);
    }
}
