<?php

namespace App\Service;

class PositionService
{
    private static array $positions = [];

    public static function addPosition(int $id): int
    {
        self::$positions[] = $id;

        return count(self::$positions);
    }

    public static function hasPosition(int $id): bool
    {
        return in_array($id, self::$positions);
    }

    public static function resetPositions(): void
    {
        self::$positions = [];
    }
}
