<?php

namespace App\WebSocket\Action;

use App\Cache\CacheHandler;
use SplObjectStorage;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

abstract class AbstractAction implements ActionInterface
{
    protected OutputInterface $output;

    public function __construct(
        #[Autowire(service: 'ratchet.web_socket.clients')]
        protected SplObjectStorage $clients,
        protected CacheHandler $cacheHandler,
    ) {
        $this->output = new NullOutput();
    }

    public function setDebugOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }
}
