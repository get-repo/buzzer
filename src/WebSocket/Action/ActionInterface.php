<?php

namespace App\WebSocket\Action;

use Ratchet\ConnectionInterface;

interface ActionInterface
{
    public function __invoke(ConnectionInterface $conn, array $args = []): void;
}
