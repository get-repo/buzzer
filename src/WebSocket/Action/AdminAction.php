<?php

namespace App\WebSocket\Action;

use Ratchet\ConnectionInterface;

class AdminAction extends AbstractAction
{
    public function __invoke(ConnectionInterface $conn, array $args = []): void
    {
        $this->cacheHandler->addAdminIdentifier($conn->resourceId);
        $this->output->writeln(sprintf('Open connection for admin #<fg=white;options=bold>%s</>', $conn->resourceId));
    }
}
