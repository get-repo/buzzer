<?php

namespace App\WebSocket\Action;

use Ratchet\ConnectionInterface;

class OpenAction extends AbstractAction
{
    public function __invoke(ConnectionInterface $conn, array $args = []): void
    {
        $this->output->writeln(sprintf('Open connection for client #<fg=white;options=bold>%s</>', $conn->resourceId));
    }
}
