<?php

namespace App\WebSocket\Action;

use App\Service\PositionService;
use Ratchet\ConnectionInterface;

class PressAction extends AbstractAction
{
    public function __invoke(ConnectionInterface $conn, array $args = []): void
    {
        $resourceId = $conn->resourceId;
        if (!PositionService::hasPosition($resourceId)) {
            $position = PositionService::addPosition($resourceId);
            $this->output->writeln(sprintf(
                '"%s" position #<fg=white;options=bold>%d</>',
                $this->cacheHandler->get($resourceId, 'name'),
                $position,
            ));
            $conn->send((string)$position);
            // send to admin
            $adminIds = $this->cacheHandler->getAdminIdentifiers();
            /** @var \Ratchet\Server\IoConnection $client */
            foreach ($this->clients as $client) {
                if (in_array($client->resourceId, $adminIds)) {
                    $client->send((string)json_encode([
                        'position' => $position,
                        'name' => $this->cacheHandler->get($conn->resourceId, 'name'),
                        'color' => $this->cacheHandler->get($conn->resourceId, 'color'),
                    ]));
                }
            }
        }
    }
}
