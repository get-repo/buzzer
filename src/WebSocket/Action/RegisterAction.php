<?php

namespace App\WebSocket\Action;

use Ratchet\ConnectionInterface;

class RegisterAction extends AbstractAction
{
    public function __invoke(ConnectionInterface $conn, array $args = []): void
    {
        $this->cacheHandler->set($conn->resourceId, 'name', $args[0] ?? '<UNKNOWN>');
        $this->cacheHandler->set($conn->resourceId, 'color', $args[1] ?? '#000');
        $this->output->writeln(sprintf('Registered client #<fg=white;options=bold>%s</>', $conn->resourceId));
    }
}
