<?php

namespace App\WebSocket\Action;

use App\Service\PositionService;
use Ratchet\ConnectionInterface;

class ResetAction extends AbstractAction
{
    public function __invoke(ConnectionInterface $conn, array $args = []): void
    {
        PositionService::resetPositions();
        // send to buzzers
        $adminIds = $this->cacheHandler ->getAdminIdentifiers();
        /** @var \Ratchet\Server\IoConnection $client */
        foreach ($this->clients as $client) {
            if (in_array($client->resourceId, $adminIds)) {
                if ($conn !== $client) {
                    $client->send('reset');
                }
            } else {
                $client->send('reset');
            }
        }
    }
}
