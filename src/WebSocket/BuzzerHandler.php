<?php

namespace App\WebSocket;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class BuzzerHandler implements MessageComponentInterface
{
    protected array $actions = [];
    protected OutputInterface $output;

    public function __construct(
        #[Autowire(service: 'ratchet.web_socket.clients')]
        private SplObjectStorage $clients,
        #[TaggedIterator('ratchet.web_socket.action')]
        iterable $actions,
    ) {
        foreach ($actions as $action) {
            preg_match('/^App\\\WebSocket\\\Action\\\([A-Za-z]+)Action/', (string)get_class($action), $matches);
            $this->actions[strtolower($matches[1])] = $action;
        }
        $this->output = new NullOutput();
    }

    public function setDebugOutput(OutputInterface $output): void
    {
        $this->output = $output;
        /** @var \App\WebSocket\Action\AbstractAction $action */
        foreach ($this->actions as $action) {
            $action->setDebugOutput($output);
        }
    }

    public function onOpen(ConnectionInterface $conn): void
    {
        $this->clients->attach($conn);
        $this->callAction($conn, 'open');
    }

    /** @param $action string */
    public function onMessage(ConnectionInterface $from, $msg): void
    {
        $args = explode(':', $msg);
        $action = array_shift($args);
        $this->callAction($from, $action, $args);
    }

    public function onClose(ConnectionInterface $conn): void
    {
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e): void
    {
        $conn->close();
        throw $e;
    }

    public function clear(): void
    {
        $this->clients->removeAll($this->clients);
    }

    private function callAction(ConnectionInterface $conn, string $action, array $args = []): void
    {
        if (!isset($this->actions[$action])) {
            throw new \InvalidArgumentException(sprintf('Action "%s" does not exists.', $action));
        }
        $this->output->writeln(sprintf(
            '<fg=yellow>Call action "<fg=yellow;options=bold>%s</>" with args %s</>',
            $action,
            json_encode($args),
        ));
        ($this->actions[$action])($conn, $args, $this->output);
    }
}
