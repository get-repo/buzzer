<?php

namespace App\Tests\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Step\Then;
use Behat\Step\When;

class FeatureContext extends MinkContext
{
    #[Then('/^print( html)?( "(?P<selector>[^"]+)")?$/')]
    public function printContext(bool $html = false, string $selector = null): void
    {
        if ($selector) {
            $content = $this->assertSession()->elementExists('css', $selector)->getHtml();
        } else {
            $content = $this->getSession()->getPage()->getHtml();
        }

        // remove <script> and <style> tags manually
        foreach (['script', 'style'] as $tag) {
            $content = preg_replace('~<(' . $tag . ')(.*?)</' . $tag . '>~Usi', "", $content);
        }
        if (!$html) {
            $content = html_entity_decode(strip_tags($content), ENT_QUOTES | ENT_HTML5, 'UTF-8');
            $content = preg_replace('/(\n+\s+)/', "\n", $content);
            $content = preg_replace('/\s{2,}/', ' ', $content);
        }

        echo trim($content);
    }

    #[When('/^(?:|I )wait "(?P<nb>[0-9]+)" second(s) $/')]
    public function iWaitSeconds(int $nb): void
    {
        usleep($nb);
    }
}
