@client
Feature: Client page

@javascript
Scenario: Client UserOne page position #1
    When I am on "/"
    Then I should be on "/setup"
        And I fill in the following:
            | client_name_form_name | UserOne |
        And I press "Let's go"
    Then I should be on "/"
        And I should see "UserOne"
        And I should see "Buzz" in the "#buzzer" element
    When I press the 1 "buzzer" button
    Then I wait 5 seconds until I see "1" in the "#position" element
    When I am on "/reset"
    Then I should be on "/setup"
