@setup
Feature: Setup page

Scenario: No session redirects to setup
    When I am on "/"
    Then the response status code should be 200
        And I should be on "/setup"

Scenario: Setup page
    When I am on "/setup"
    Then the response status code should be 200
        And I should see "Buzzer - Set up"
        And I should see "Let's go" in the "#client_name_form_submit" element

@javascript
Scenario Outline: Invalid names
    When I am on "/setup"
        And I fill in the following:
            | client_name_form_name | <name> |
        And I press "Let's go"
    Then I should be on "/setup"
    Examples:
        | name        |
        | Inv@l1d     |
        | some space  |
        | _underscore |

@javascript
Scenario: Create name NameForUsed
    When I am on "/setup"
    And I fill in the following:
        | client_name_form_name | NameForUsed |
    And I press "Let's go"
    Then I should be on "/"

@javascript
Scenario: NameForUsed already exists
    When I am on "/setup"
        And I fill in the following:
            | client_name_form_name | NameForUsed |
        And I press "Let's go"
    Then I should be on "/setup"
        And I should see "This name is already used"
    When I am on "/reset"
    Then I should be on "/setup"

@javascript
Scenario: Valid name
    When I am on "/setup"
        And I fill in the following:
            | client_name_form_name | Test |
        And I press "Let's go"
    Then I should be on "/"
    When I am on "/reset"
    Then I should be on "/setup"
