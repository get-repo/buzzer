<?php

namespace App\Tests\Functional;

use Symfony\Component\Panther\Client;
use Symfony\Component\Panther\PantherTestCase;

class ScenarioTest extends PantherTestCase
{
    public const WEB_SERVER_HOST = '127.0.0.1';
    public const WEB_SERVER_PORT = 9080;
    public const CHROME_PORT = 9515;
    public const BUTTON = "Let's go";

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::startWebServer([
            'hostname' => self::WEB_SERVER_HOST,
            'port' => self::WEB_SERVER_PORT,
            'env' => 'test',
        ]);
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        static::stopWebServer();
        shell_exec(sprintf('rm -fr "%s"', realpath(__DIR__ . '/../../var/sessions/panther')));
    }

    public function testNameAlreadyUsed(): void
    {
        $client1 = self::createChromeClient();
        $client1->followRedirects();
        $client1->request('GET', '/setup');
        $crawler = $client1->submitForm(self::BUTTON, ['client_name_form[name]' => 'TheName']);
        $this->assertStringContainsString("THENAME\nBuzz", $crawler->text());

        $client2 = self::createChromeClient();
        $client2->followRedirects();
        $client2->request('GET', '/setup');
        $crawler = $client2->submitForm(self::BUTTON, ['client_name_form[name]' => 'TheName']);
        $this->assertStringContainsString('This name is already used', $crawler->text());

        $client1->getCookieJar()->clear();
        $client2->getCookieJar()->clear();
    }

    public function testPositionOrderAdmin(): void
    {
        $keys = ['AAA', 'BBB', 'CCC'];

        $admin = self::createChromeClient();
        $admin->followRedirects();
        $admin->request('GET', '/admin');

        $clients = [];
        foreach ($keys as $key) {
            $client = self::createChromeClient();
            $client->followRedirects();
            $client->request('GET', '/setup');
            $crawler = $client->submitForm(self::BUTTON, ['client_name_form[name]' => $key]);
            $this->assertStringContainsString("{$key}\nBuzz", $crawler->text());
            $clients[$key] = $client;
        }

        // shuffle array
        shuffle($keys);

        $expected = [];
        foreach ($keys as $i => $key) {
            /** @var Client $client */
            $client = $clients[$key];
            $crawler = $client->getCrawler();
            $crawler->filter('#buzzer')->click();
            $client->waitForDisabled('#buzzer', 5);
            $client->waitForVisibility('#position', 5);
            $this->assertCount($i ? 0 : 1, $crawler->filter('body.winner'));
            $this->assertCount($i ? 1 : 0, $crawler->filter('body.too-late'));
            $expected[] = sprintf('%d - %s', ++$i, $key);
        }

        $content = $admin->refreshCrawler()->text();
        $admin->getCrawler()->filter('#reset')->click();
        $this->assertStringContainsString(implode(PHP_EOL, $expected), $content);

        $admin->getCookieJar()->clear();
        foreach ($keys as $key) {
            /** @var Client $client */
            $client = $clients[$key];
            $client->getCookieJar()->clear();
        }
    }

    private static function createChromeClient(): Client
    {
        if (isset($GLOBALS['_CHROME_PORT'])) {
            $GLOBALS['_CHROME_PORT'] = $GLOBALS['_CHROME_PORT'] + 1;
        } else {
            $GLOBALS['_CHROME_PORT'] = self::CHROME_PORT;
        }
        return Client::createChromeClient(
            baseUri: self::getBaseUri(),
            options: [
                'port' => $GLOBALS['_CHROME_PORT'],
                'capabilities' => [ /** @see https://github.com/symfony/panther/issues/339#issuecomment-757553292 */
                    'goog:loggingPrefs' => ['browser' => 'ALL'],
                ],
            ],
        );
    }

    private static function getBaseUri(): string
    {
        return sprintf('http://%s:%s', self::WEB_SERVER_HOST, self::WEB_SERVER_PORT);
    }
}
