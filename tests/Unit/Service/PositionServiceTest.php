<?php

namespace Unit\Service;

use App\Service\PositionService;
use PHPUnit\Framework\TestCase;

class PositionServiceTest extends TestCase
{
    public function test(): void
    {
        $this->assertFalse(PositionService::hasPosition(999));
        $this->assertEquals(1, PositionService::addPosition(999));
        $this->assertTrue(PositionService::hasPosition(999));
        $this->assertEquals(2, PositionService::addPosition(888));
        $this->assertTrue(PositionService::hasPosition(888));
        PositionService::resetPositions();
        $this->assertFalse(PositionService::hasPosition(999));
        $this->assertFalse(PositionService::hasPosition(888));
    }
}
